from mip import Model, xsum


# A very simple linear model for storage
def linear_model_storage(n_period=10,
                         capacity=40,
                         p_max_charge=40,
                         p_max_discharge=40,
                         delta_t=1,
                         soc_ini=0.5,
                         soc_min=0.1,
                         soc_max=0.9,
                         eff_charge=0.95,
                         eff_dis_charge=0.9,
                         aging_cycle_cost=1,
                         relaxation_soc_ini=True):
    # Define the model of storage
    m = Model()
    p_charge = {k: m.add_var(lb=0, ub=p_max_charge) for k in range(n_period)}
    p_dis_charge = {k: m.add_var(lb=0, ub=p_max_discharge) for k in range(n_period)}
    # In the case SOC ini is not in range of SOC-min and SOC-max, a relaxation is necessary for avoiding the unfeasible
    # problem
    if relaxation_soc_ini:
        e = {k: m.add_var(lb=min(soc_ini, soc_min) * capacity,
                          ub=max(soc_ini, soc_max) * capacity)
             for k in range(n_period)}
    else:
        e = {k: m.add_var(lb=soc_min * capacity,
                          ub=soc_max * capacity)
             for k in range(n_period)}

    storage_cost = m.add_var(lb=0)

    # SOC Evolution constraint
    for k in range(n_period):
        if k == 0:
            m += e[k] == soc_ini * capacity, 'SOC init'
        elif k >= 1:
            m += e[k] == e[k - 1] \
                + delta_t * eff_charge * p_charge[k - 1] \
                - delta_t * p_dis_charge[k - 1] / eff_dis_charge

    #  Make sure the end of period there are no power of discharge or charge
    m += p_charge[n_period - 1] == 0
    m += p_dis_charge[n_period - 1] == 0
    #  Storage cost
    cost_of_kwh = aging_cycle_cost / capacity
    m += storage_cost == xsum(cost_of_kwh * (p_charge[k] * eff_charge + p_dis_charge[k] / eff_dis_charge)
                              for k in range(n_period))
    return m, e, p_charge, p_dis_charge, storage_cost

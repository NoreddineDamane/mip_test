from app.LinearModelStorage import linear_model_storage
from mip import maximize, xsum, OptimizationStatus, minimize
import unittest


class Test(unittest.TestCase):
    def test_linear_model_storage(self):

        k = 10  # Number total of time step
        cap = 40
        soc_min_val = 0.1
        print('In this Test, We try maximizing the injection of energy to the grid, '
              'the expecting result is the storage is SOC min in the end of period ')
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=cap,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=0.5,
                                                                          soc_min=soc_min_val,
                                                                          soc_max=0.9,
                                                                          eff_charge=0.95,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=1,
                                                                          relaxation_soc_ini=False)
        p_grid = {k: m.add_var(lb=-80, ub=80) for k in range(k)}
        for t in range(k):
            m += p_grid[t] == p_dis_charge[t] - p_charge[t]
        # Maximizing the total energy to the grid
        epsilon = 1e-3
        m.objective = maximize(xsum(p_grid[t] - epsilon * (p_dis_charge[t] + p_charge[t])
                                    for t in range(k)))
        status = m.optimize(max_seconds=300)
        if status == OptimizationStatus.OPTIMAL:
            print('SOC End of storage is  {} kWh'.format(e[9].x))
        else:
            self.fail('Cannot reach the optimal solution')
        # Verification if the storage is empty in the end of period
        print('P_grid')
        for t in range(k):
            print(p_grid[t].x)

        print('P_charge')
        for t in range(k):
            print(p_charge[t].x)

        print('P_dis_charge')
        for t in range(k):
            print(p_dis_charge[t].x)

        self.assertEqual(e[9].x, cap * soc_min_val)

    def test_linear_model_storage_1(self):
        print('In this Test, We try minimizing the injection of energy to the grid, '
              'the expecting result is the storage is full(SOC max) in the end of period ')
        k = 10  # Number total of time step
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=0.5,
                                                                          soc_min=0.1,
                                                                          soc_max=0.9,
                                                                          eff_charge=0.95,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=1,
                                                                          relaxation_soc_ini=False)
        p_grid = {k: m.add_var(lb=-80, ub=80) for k in range(k)}
        for t in range(k):
            m += p_grid[t] == p_dis_charge[t] - p_charge[t]
        # Maximizing the end soc
        epsilon = 1e-1
        m.objective = minimize(xsum(p_grid[t] + epsilon * (p_dis_charge[t] + p_charge[t])
                                    for t in range(k)))
        status = m.optimize(max_seconds=300)
        if status == OptimizationStatus.OPTIMAL:
            print('SOC End of storage is  {} kWh'.format(e[9].x))
        else:
            self.fail('Cannot reach the optimal solution')
        # Verification if the storage is empty in the end of period
        print('P_grid')
        for t in range(k):
            print(p_grid[t].x)

        print('P_charge')
        for t in range(k):
            print(p_charge[t].x)

        print('P_dis_charge')
        for t in range(k):
            print(p_dis_charge[t].x)
        self.assertEqual(e[9].x, 40 * 0.9)

    def test_linear_model_storage_aging_charge(self):
        print('****************************************************************\n'
              'In this Test, We check if the aging cost is correly implemented '
              'in this optimization, the storage will full charge one cycle. '
              'The cost of using battery should be 1€ in this example.\n '
              '****************************************************************')
        k = 10  # Number total of time step
        aging_cycle_cost = 1
        eff_charge = 0.95
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=0,
                                                                          soc_min=0,
                                                                          soc_max=1,
                                                                          eff_charge=eff_charge,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=aging_cycle_cost,
                                                                          relaxation_soc_ini=False
                                                                          )
        m.objective = maximize(e[k - 1])
        status = m.optimize()
        print('Energy')
        for t in range(k):
            print(e[t].x)
        print('P_out')
        for t in range(k):
            print(p_dis_charge[t].x - p_charge[t].x)
        if status == OptimizationStatus.OPTIMAL:
            print('Expecting the cost of using storage is {} €'.format(aging_cycle_cost))
            print('Cost of storage using after optimization {} €'.format(storage_cost.x))
        else:
            self.fail('Cannot reach the optimal solution')
        self.assertEqual(storage_cost.x, aging_cycle_cost)

    def test_linear_model_storage_aging_discharge(self):
        print('****************************************************************\n'
              'In this Test, We check if the aging cost is correctly implemented '
              'in this optimization, the storage will full discharge one cycle. '
              'The cost of using battery should be 1€ in this example.\n '
              '****************************************************************')
        k = 10  # Number total of time step
        aging_cycle_cost = 1
        eff_charge = 0.95
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=1,
                                                                          soc_min=0,
                                                                          soc_max=1,
                                                                          eff_charge=eff_charge,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=aging_cycle_cost,
                                                                          relaxation_soc_ini=False
                                                                          )
        m.objective = minimize(e[k - 1])
        status = m.optimize()
        print('Energy')
        for t in range(k):
            print(e[t].x)
        print('P_out')
        for t in range(k):
            print(p_dis_charge[t].x - p_charge[t].x)
        if status == OptimizationStatus.OPTIMAL:
            print('Expecting the cost of using storage is {} €'.format(aging_cycle_cost))
            print('Cost of storage using after optimization {} €'.format(storage_cost.x))
        else:
            self.fail('Cannot reach the optimal solution')
        self.assertEqual(storage_cost.x, aging_cycle_cost)

    def test_linear_model_storage_soc_relaxation_soc_ini_smaller_than_soc_min(self):
        print('****************************************************************\n'
              'In this Test, We check if the relaxation is correctly implemented '
              'in this optimization, the storage will have soc_ini = 0, soc_min = 0.1. '
              'With option relaxation_soc_ini true, the problem still be sold to optimal.\n '
              '****************************************************************')
        k = 10  # Number total of time step
        aging_cycle_cost = 1
        eff_charge = 0.95
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=0,
                                                                          soc_min=0.1,
                                                                          soc_max=1,
                                                                          eff_charge=eff_charge,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=aging_cycle_cost,
                                                                          relaxation_soc_ini=True
                                                                          )
        m.objective = maximize(e[k - 1])
        status = m.optimize()
        print('Energy')
        for t in range(k):
            print(e[t].x)
        print('P_out')
        for t in range(k):
            print(p_dis_charge[t].x - p_charge[t].x)
        if status == OptimizationStatus.OPTIMAL:
            print('Expecting the problem is solvable !')
            print('Status of problem {}'.format(status))
        else:
            self.fail('Cannot reach the optimal solution')

    def test_linear_model_storage_soc_relaxation_soc_ini_greater_than_soc_min(self):
        print('****************************************************************\n'
              'In this Test, We check if the relaxation is correctly implemented '
              'in this optimization, the storage will have soc_ini = 1, soc_max = 0.9. '
              'With option relaxation_soc_ini true, the problem still be sold to optimal.\n '
              '****************************************************************')
        k = 10  # Number total of time step
        aging_cycle_cost = 1
        eff_charge = 0.95
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=1,
                                                                          soc_min=0.1,
                                                                          soc_max=0.9,
                                                                          eff_charge=eff_charge,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=aging_cycle_cost,
                                                                          relaxation_soc_ini=True
                                                                          )
        m.objective = minimize(e[k - 1])
        status = m.optimize()
        print('State of Energy')
        for t in range(k):
            print(e[t].x)
        print('P_out')
        for t in range(k):
            print(p_dis_charge[t].x - p_charge[t].x)
        if status == OptimizationStatus.OPTIMAL:
            print('Expecting the problem is solvable !')
            print('Status of optimzation {}'.format(status))
        else:
            self.fail('Cannot reach the optimal solution')

    def test_linear_model_storage_soc_relaxation_soc_ini_smaller_than_soc_min_infeasible(self):
        print('****************************************************************\n'
              'In this Test, We check if the relaxation is correctly implemented '
              'in this optimization, the storage will have soc_ini = 0, soc_min = 0.1. '
              'With option relaxation_soc_ini true, the problem still be sold to optimal.\n '
              '****************************************************************')
        k = 10  # Number total of time step
        aging_cycle_cost = 1
        eff_charge = 0.95
        m, e, p_charge, p_dis_charge, storage_cost = linear_model_storage(n_period=k,
                                                                          capacity=40,
                                                                          p_max_charge=40,
                                                                          p_max_discharge=40,
                                                                          delta_t=1,
                                                                          soc_ini=0,
                                                                          soc_min=0.1,
                                                                          soc_max=1,
                                                                          eff_charge=eff_charge,
                                                                          eff_dis_charge=0.9,
                                                                          aging_cycle_cost=aging_cycle_cost,
                                                                          relaxation_soc_ini=False
                                                                          )
        m.objective = maximize(e[k - 1])
        status = m.optimize()
        if status == OptimizationStatus.INFEASIBLE:
            print('Expecting the problem is INFEASIBLE !')
            print('Status of optimization {}'.format(status))
        else:
            self.fail('The status is different than INFEASIBLE')